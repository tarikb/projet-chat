<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


/**
 * @Route("/message", name="message")
 */
class MessageController extends Controller
{
    /**
     * @Route("/", name="message")
     */
    public function index()
    {
        return $this->render('message/index.html.twig', [
            'controller_name' => 'MessageController',
        ]);
    }

    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }


    /**
     * @Route("/getmessage", name="recupère les messages", methods={"GET"})
     */
    public function getMessages() {
        $emails = $this->getDoctrine()->getRepository(message::class)
            ->findAll();
        $data = $this->serializer->serialize($emails, 'json');

        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json");
        return new Response($data);
    }

    /**
     * @Route("/addMessage", name="ajoute un message", methods="POST")
     */
    public function addMessage($id=1)
    {
        $request = Request::createFromGlobals();
        $content = $request->request->get('content');
        $user= $this->getDoctrine()->getRepository(User::class)->find($id);
        $message = new Message();
        $message->setContent($content);
        $message->setUser($user);
        $data= ["success"=>'message: '.$content];
        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        return new JsonResponse($data);

    }
}
