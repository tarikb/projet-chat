<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/user", name="user", methods={"GET"})
 */
class UserController extends Controller
{
    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }
    /**
     * @Route("/adduser/", name="add_user")
     */
    public function addUser()

    {
        $request = Request::createFromGlobals();
        $users = $request->request->get('user');

        $user = new User();

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();


        return new JsonResponse($user);
    }
}
